# Code Quality

Use Code Quality to analyze your source code’s quality and complexity. This helps keep your project’s code simple, readable, and easier to maintain. Code Quality should supplement your other review processes, not replace them.

Code Quality uses the open source Code Climate tool, and selected plugins, to analyze your source code. To confirm if your code’s languages are covered, see the Code Climate list of Supported Languages for Maintainability. You can extend the code coverage either by using Code Climate Analysis Plugins or a custom tool.

Run Code Quality reports in your CI/CD pipeline to verify changes don’t degrade your code’s quality, before committing them to the default branch.

Read more about Code Quality on GitLab at https://docs.gitlab.com/ee/ci/testing/code_quality.html

## Usage

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/code-quality@main

stages:
  - test # ensure that at least `test` stage exists
```

A `code_quality` job will be added to the pipeline in the `test` stage.